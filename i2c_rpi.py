from smbus import SMBus
try:
    import mux
except:
    print("not using pihat")

class i2c_rpi:
    def __init__(self,address,channel=None):
        self.address=address #i2c address
        self.channel=channel #pihat channel
        self.bus=SMBus(1)
        
    def write(self,register,data):
        try: #if you don't want to use the pihat and just connect it to gpio
            mux.select_channel(self.channel)
        except:
            pass
        self.bus.write_byte_data(self.address,register,data)


    def read(self,register):
        try:
            mux.select_channel(self.channel)
        except:
            pass
        
        data=self.bus.read_byte_data(self.address,register)
        return data
    

if __name__=="__main__":
    comm=i2c_rpi(0)
    comm.write(44,0)
